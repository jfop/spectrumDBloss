def Calculate():
    splitter5 = 0
    splitter6 = 0
    splitter7 = 0
    splitter8 = 0
    
    #inputs in python are received as strings, need to convert to modify
    #ints are only whole numbers(1,213,34,34), float type variable accepts decimals(1.4334,  45.234)
    #python is a dynamic language so variable types are automatically assigned to the correct type 

    choice = int(input("DB(1) or TX(2): "))
    startingDB = float(input("Starting DB: "))
    totalLengthRG11 = float(input("Total length of wires over 150: "))
    lossPer100RG11 = float(input("Loss per 100RG11: "))
    totalLengthRG6 = float(input("Total length of wires under 150: "))
    lossPer100RG6 = float(input("Loss per 100RG6: "))
    splitter2 = int(input("2 way splitter count: "))
    splitter3 = int(input("3 way splitter count: "))
    splitter4 = int(input("4 way splitter count: "))
    
    
        #Checks to see if there are any unbalanced splitters

    unbalancedChoice = input("Are there any unbalanced splitter? (Yes/No): ")
    if unbalancedChoice == "Yes" or unbalancedChoice == "yes" or unbalancedChoice == "y" or unbalancedChoice == "Y":
      unbalancedSplitterChoice = int(input("Is the splitter 2(2) or 3(3) way?: "))
      if unbalancedSplitterChoice == 2:
        splitter5 = int(input("2 way unbalanced (1 DB loss) count: "))
        splitter6 = int(input("2 way unbalanced (6 DB loss) count: "))
      if unbalancedSplitterChoice == 3:
        splitter7 = int(input("2 way unbalanced (3 DB loss) count: "))
        splitter8 = int(input("2 way unbalanced (7 DB loss) count: "))
        
    #Finds DB or TX based on unbalancedChoice
    if choice == 1:
        totalLoss = ((totalLengthRG11/100) * lossPer100RG11)
        totalLoss = totalLoss + ((totalLengthRG6/100) * lossPer100RG6)
        totalLoss = totalLoss + (splitter2 * 3.5)
        totalLoss = totalLoss + (splitter3 * 5.5)
        totalLoss = totalLoss + (splitter4 * 7)
        totalLoss = totalLoss + (splitter5 * 1)
        totalLoss = totalLoss + (splitter6 * 6)
        totalLoss = totalLoss + (splitter7 * 3)
        totalLoss = totalLoss + (splitter8 * 7)
        endingDB = startingDB - totalLoss
        print ("Ending DB: ",endingDB)
    if choice == 2:
        totalLoss = ((totalLengthRG11/100) * lossPer100RG11)
        totalLoss = totalLoss + ((totalLengthRG6/100) * lossPer100RG6)
        totalLoss = totalLoss + (splitter2 * 3.5)
        totalLoss = totalLoss + (splitter3 * 5.5)
        totalLoss = totalLoss + (splitter4 * 7)
        totalLoss = totalLoss + (splitter5 * 1)
        totalLoss = totalLoss + (splitter6 * 6)
        totalLoss = totalLoss + (splitter7 * 3)
        totalLoss = totalLoss + (splitter8 * 7)
        endingDB = startingDB - totalLoss
        print ("Ending DB: ",endingDB)
Calculate()
